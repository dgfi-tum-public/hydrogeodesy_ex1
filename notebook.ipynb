{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Hydrogeodesy: Monitoring surface waters from space\n",
    "### Exercise 1: From measured ranges to water level & pre-processed inland altimetry products\n",
    "\n",
    "Daniel Scherer, DGFI-TUM  \n",
    "Wintersemester 2024/25\n",
    "\n",
    "##### **Contents**\n",
    "1. Height estimation and retracker comparison\n",
    "2. Water level time series\n",
    "3. Qaulity Assessment\n",
    "\n",
    "In this Exercise you learn how to estimate water levels from classical nadir satellite altimetry data.<br>\n",
    "We will use the Envisat, Sentinel-3A/B, and Jason/Sentinel-6A missions to estimate water levels at the Sam Rayburn Reservoir in Texas, USA.<br>\n",
    "The data is pre-processed and provided in the `data` folder.\n",
    "\n",
    "##### **Study Area: Sam Rayburn Reservoir (Texas, USA)**\n",
    "The Sam Rayburn Reservoir is located in Texas, USA.<br>\n",
    "It is crossed by the Envisat, Sentinel-3A/B, and Jason/Sentinel-6A missions, so we can compare the quality of the different missions over decades.<br>\n",
    "There is also an in-situ water level gauge station at the reservoir, which we can use to validate our estimates.<br>\n",
    "The water level varies by  approx. 7 m (!), so we can expect to see a strong signal in the satellite derived water level timeseries.\n",
    "<!-- ![AOI](Map.png)   -->\n",
    "<img src=\"Sam Rayburn.png\" alt=\"Map\" style=\"width: 300mm;\"/>\n",
    "\n",
    "*Figure 1: Sam Rayburn Reservoir and the intersecting **nominal** groundtracks of the Envisat, Sentinel-3A/B, and Jason-3 Misssions.<br>**Note that all Jason and the Sentinel-6A missions share the same orbit!***  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Excercise\n",
    "\n",
    "First, we will import the necessary Python libraries and load the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd # Pandas is a powerful tool for data analysis\n",
    "import geopandas as gpd # Geopandas is an extension to pandas that makes working with geospatial data easier\n",
    "import matplotlib.pyplot as plt # Matplotlib is a standard plotting library in Python\n",
    "import contextily as cx # Contextily is a package to add basemaps to your plots\n",
    "import cartopy.crs as ccrs # Cartopy is a package for geospatial data visualization\n",
    "from matplotlib_scalebar.scalebar import ScaleBar # Scalebar is a package to add scalebars to your plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The satellite altimetry data for this exercise is provided in the *raw_data.gpkg* file.\n",
    "\n",
    "Run the following cell to load the data and take a look at its structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = gpd.read_file('data/SamRayburn/raw_data.gpkg')\n",
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data is now stored in a **geopandas dataframe** with the following columns:\n",
    "\n",
    "| Column Name | Description |\n",
    "| -------- | ------- |\n",
    "| datetime | Date and time of the measurement |\n",
    "| glon | Longitude of the measurement |\n",
    "| glat | Latitude of the measurement |\n",
    "| misssion | Satellite mission name |\n",
    "| cycle_nr | Cycle number |\n",
    "| pass_nr | Pass number |\n",
    "| hsat | Satellite height above the ellipsoid |\n",
    "| uralt | Uncorrected range to the satellite |\n",
    "| ionos| Ionospheric correction |\n",
    "| wtrop | Wet tropospheric correction |\n",
    "| dtrop | Dry tropospheric correction |\n",
    "| ptide | Ocean tide correction |\n",
    "| etide | Solid earth tide correction |\n",
    "| geoh | Geoid height |\n",
    "| ralt_ocean | Estimated range by the ocean retracker |\n",
    "| ralt_ice | Estimated range by the ice retracker |\n",
    "| ralt_imth | Estimated range by the improved threshold retracker |\n",
    "| oerr | Radial Orbit Errors (Intermission biases) |\n",
    "| geometry | Coordinates of the measurement as python shypely Point object |\n",
    "\n",
    "The columns can be accessed using the following syntax:\n",
    "\n",
    "```python\n",
    "# Accessing the datetime column\n",
    "data.datetime\n",
    "```\n",
    "or\n",
    "```python\n",
    "data['datetime']\n",
    "```\n",
    "which returns a vector:\n",
    "```\n",
    "0        2002-07-12 03:56:07\n",
    "1        2002-07-12 03:56:07\n",
    "2        2002-07-12 03:56:07\n",
    "3        2002-07-12 03:56:07\n",
    "4        2002-07-12 03:56:08\n",
    "                 ...       \n",
    "```\n",
    "\n",
    "with the following line you can display statistics of the data:\n",
    "\n",
    "```python\n",
    "data.describe()\n",
    "```\n",
    "\n",
    "**Which geophysical correction is most significant?**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.describe().round(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The geopandas data can also be plotted directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(10, 10), subplot_kw={'projection': ccrs.epsg(3857)})\n",
    "colors = {'S3B':'#009900', 'envisat':'#1f78b4'}\n",
    "for name, group in data.to_crs(3857).groupby('mission'):\n",
    "    group.plot(ax=ax, markersize=2, label=name, color=colors.get(name, '#db1e2a'))\n",
    "\n",
    "cx.add_basemap(plt.gca(), source=cx.providers.USGS.USImagery, alpha=0.75, crs=3857,attribution='')\n",
    "\n",
    "gl = ax.gridlines(draw_labels=True, color='black', alpha=0.5, linestyle='--')\n",
    "\n",
    "plt.legend(title='Mission', loc='upper left')\n",
    "ax.add_artist(ScaleBar(1))\n",
    "plt.xticks([])\n",
    "plt.yticks([])\n",
    "plt.title('Figure 2: Measured locations of the different missions')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note, how the actual data deviate from the nominal groundtrack (Figure 1) as the satellites drift over time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.1 Height estimation and retracker comparison\n",
    "\n",
    "New columns can be added to the dataframe using the following syntax:\n",
    "\n",
    "```python\n",
    "# Adding a new column to the dataframe\n",
    "data['new_column'] = new_values\n",
    "```\n",
    "\n",
    "The columns are treated as vectors, so you can perform mathematical operations on them directly:\n",
    "\n",
    "```python\n",
    "# Adding two columns together\n",
    "data['new_column'] = data['column1'] - data['column2']\n",
    "```\n",
    "| Index | new_column |   =   | column1  |   -   | column2  |\n",
    "|-------|------------|-------|----------|-------|----------|\n",
    "| 0     | -2.0       |       | 3.0      |       |  1.0     |\n",
    "| 1     |  0.0       |       | 2.0      |       |  2.0     |\n",
    "| 2     |  NaN       |       | 1.0      |       |  NaN     |\n",
    "| 3     |  6.0       |       | -2.0     |       |  4.0     |\n",
    "| 4     |  NaN       |       | NaN      |       |  5.0     |\n",
    "\n",
    "Note, that they must use a common index.\n",
    "\n",
    "**Calculate the Water Surface Elevation (WSE) using the following formula:**\n",
    "\n",
    "$$\n",
    "h = hsat - (R + corrections)\n",
    "$$\n",
    "\n",
    "with\n",
    "- $h$: resulting water height (WSE)\n",
    "- $R$: Range from the retracker algorithm\n",
    "- $corrections$: geophysical corrections (cf. table above)\n",
    "\n",
    "Add new columns to the dataframe containing the water surface elevation estimates using the **ocean, ice, and improved threshold** retrackers ($h$) (**Remember to apply all the corrections including the intermission biases (oerr)**):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Replace the ...\n",
    "data['wse_ocean'] = ...\n",
    "data['wse_ice'] = ...\n",
    "data['wse_imth'] = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the water surface elevation estimates from the different retracker algorithms by running the follwoing cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the data for the Jason-2 mission, cycle 150\n",
    "cycle_subset = data.query('mission == \"jason3\" & cycle_nr == 150')\n",
    "cycle_subset.set_index('glat').sort_index()[['wse_ocean','wse_ice','wse_imth']].plot(marker='.', linestyle='-', figsize=(10, 5))\n",
    "plt.title(f'Figure 3: Water Surface Elevation for the {cycle_subset.mission.iloc[0].title()} mission, cycle {cycle_subset.cycle_nr.iloc[0]}')\n",
    "plt.legend(title='Retracker')\n",
    "plt.xlabel('Latitude')\n",
    "plt.ylabel('Water Surface Elevation [m]')\n",
    "plt.grid()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try different missions ('envisat', 'jason2', 'jason3', 'S3B', 'sentinel6a') and cycles to see how the water surface elevation estimates of the different retracker vary over time.<br>\n",
    "- **How do the retracker estimates compare to each other?** Number of observations, location of the measurements, scatter, etc.\n",
    "- **What physical effects can you observe?**\n",
    "- **How can we obtain a daily water level time series from the data?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2 Water level time series\n",
    "\n",
    "For validation and as reference, read the data from the insitu station:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "insitu_wse = pd.read_csv('data/SamRayburn/insitu_wse.csv', comment='#', names=['date','insitu_wse'], delimiter=' ', usecols=[0,1], parse_dates=['date'], index_col=0).insitu_wse\n",
    "insitu_wse.plot(figsize=(12, 5))\n",
    "plt.ylabel('Water Surface Elevation [m]')\n",
    "plt.title('Figure 4: Water Surface Elevation measured In-Situ at a Gauge Station')\n",
    "plt.grid()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to create such a time series from the satellite data using the impoved threshold retracker.<br>\n",
    "For this, we need to define oulier conditions and calculate a single water level estimate for each observed day.\n",
    "\n",
    "The Results in Figure 3 contain a lot of NaN values or Outlier, because the retracker algorithms are not able to estimate the water surface elevation over land.<br>\n",
    "Define latitude bounds to filter the data for valid measurements over the water body (cf. Figures 1, 2, and 3):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "min_envisat_lat = ...\n",
    "max_envisat_lat = ...\n",
    "\n",
    "min_sentinel3_lat = ...\n",
    "max_sentinel3_lat = ...\n",
    "\n",
    "min_jason_lat = ...\n",
    "max_jason_lat = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on Figure 4, define thresholds for the water surface elevation estimates to filter out outliers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "max_wse = ...\n",
    "min_wse = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We flag the data based on the given bounds:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data['outlier'] = True\n",
    "data.loc[(data.mission == 'envisat') & (data.glat > min_envisat_lat) & (data.glat < max_envisat_lat) & (data.wse_imth > min_wse) & (data.wse_imth < max_wse) , 'outlier'] = False\n",
    "data.loc[(data.mission == 'S3B') & (data.glat > min_sentinel3_lat) & (data.glat < max_sentinel3_lat) & (data.wse_imth > min_wse) & (data.wse_imth < max_wse), 'outlier'] = False\n",
    "data.loc[(data.mission.isin(['jason2','jason3','sentinel6a'])) & (data.glat > min_jason_lat) & (data.glat < max_jason_lat) & (data.wse_imth > min_wse) & (data.wse_imth < max_wse), 'outlier'] = False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot all the data from the improved threshold retracker not flagged as outlier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(20, 5))\n",
    "# The ~ operator inverts the boolean values in the Series so that we filter out all outliers\n",
    "for mission, group in data[~data.outlier].groupby('mission'):\n",
    "    # We calculate the median of the water surface elevation for each datetime\n",
    "    # First, we set the index of the data to the datetime column\n",
    "    # Then, we resample the data to daily (1D) values and calculate the daily median\n",
    "    group.set_index('datetime').wse_imth.resample('1D').median().dropna().plot(ax=ax, marker='.', label=mission)\n",
    "# We plot the in-situ data as reference\n",
    "insitu_wse.plot(ax=ax, zorder=-1, label='In-Situ', color='black',alpha=0.25)\n",
    "plt.legend()\n",
    "plt.grid()\n",
    "plt.ylabel('Water Surface Elevation [m]')\n",
    "plt.xlabel('Date')\n",
    "plt.title('Figure 5: Water Surface Elevation for the different missions')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- **How would you rate the resulting time series?**\n",
    "- **What are the main sources of error in the water level estimates?**\n",
    "- **How can we mitigate some remaining outliers?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.3 Quality Assessment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we will calculate the root mean square error (RMSE), Nash-Sutcliffe Efficiency (NSE), and Pearson's Correlation (R) between the satellite and insitu water level estimates to assess the quality of the satellite data.\n",
    "\n",
    "Note, that there is still a **bias** between the satellite and insitu water level estimates visible in Figure 5.<br>\n",
    "This is caused by the different vertical reference used by the insitu data.\n",
    "\n",
    "We first need to remove this bias:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the daily median water surface elevation timeseries from all missions combined\n",
    "satellite_ts = data[~data.outlier].set_index('datetime').wse_imth.resample('1D').median().dropna()\n",
    "\n",
    "# Calculate the constant bias between the satellite and in-situ data. The in-situ data is already sampled daily\n",
    "bias = (satellite_ts - insitu_wse).median()\n",
    "print(f'The bias between the satellite and in-situ data is {bias:.3f} meters')\n",
    "\n",
    "# Remove the bias from the satellite data\n",
    "satellite_ts = satellite_ts - bias"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now create a function to calculate the RMSE between the satellite and insitu water level estimates:\n",
    "\n",
    "The RMSE is calculated as:\n",
    "\n",
    "$$\n",
    "\\text{RMSE} = \\sqrt{\\frac{1}{n} \\sum_{i=1}^{n} (p_i - t_i)^2}\n",
    "$$\n",
    "\n",
    "Where:\n",
    "- $n$: Number of data points\n",
    "- $p_i$: Predicted value at index $i$\n",
    "- $t_i$: True value at index $i$\n",
    "\n",
    "**Note:**\n",
    " - Remember how we calculated the bias in the previous step. You can use a similar notation but use the ```mean()``` instead of the ```median()```.\n",
    " - You can square a series using the ```**``` operator. For example, ```x**2``` squares the value of series x. You can also use ```x.pow(2)```.\n",
    " - We import the numpy module as np as you might need the ```np.sqrt(x)``` function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "def calc_rmse(predictions, truth):\n",
    "    rmse = ...\n",
    "    return rmse\n",
    "\n",
    "rmse = calc_rmse(satellite_ts, insitu_wse)\n",
    "print(f'The RMSE between the satellite and in-situ data is {rmse:.3f} meters')\n",
    "\n",
    "print('-----')\n",
    "for mission, group in data[~data.outlier].groupby('mission'):\n",
    "    mission_ts = group.set_index('datetime').wse_imth.resample('1D').median().dropna()\n",
    "    mission_bias = (mission_ts - insitu_wse).median()\n",
    "    mission_ts = mission_ts - bias\n",
    "    mission_rmse = calc_rmse(mission_ts, insitu_wse)\n",
    "    print(f'RMSE for {mission}: {mission_rmse:.3f} meters')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Are the RMSE values as expected?**\n",
    "\n",
    "Additionally, calculate the Nash-Sutcliffe Efficiency (NSE) between the satellite and insitu water level estimates:\n",
    "\n",
    "The NSE is calculated as:\n",
    "\n",
    "$$\n",
    "\\text{NSE} = 1 - \\frac{\\sum_{i=1}^{n} (p_i - t_i)^2}{\\sum_{i=1}^{n} (t_i - \\bar{t})^2}\n",
    "$$\n",
    "\n",
    "Where:\n",
    "- $n$: Number of data points\n",
    "- $p_i$: Predicted value at index $ i $\n",
    "- $t_i$: Observed (true) value at index $ i $\n",
    "- $\\bar{t}$: Mean of the observed values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calc_nse(predictions, truth):\n",
    "    # We need to align the data for this calculation as the length of truth values must match the satellite values for a representative mean.\n",
    "    # Aligning the data makes sure that both series have the same length and the same index. Data that is not present in both series is removed.\n",
    "    truth, predictions = truth.align(predictions, join='inner')\n",
    "    nse = ...\n",
    "    return nse\n",
    "\n",
    "nse = calc_nse(satellite_ts, insitu_wse)\n",
    "print(f'The NSE between the satellite and in-situ data is {nse:.3f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now calculate the Pearson's Correlation (R) between the satellite and insitu water level estimates:\n",
    "\n",
    "The Pearson's Correlation is calculated as:\n",
    "\n",
    "$$\n",
    "\\text{R} = \\frac{covariance(p,t)}{std(p) \\cdot std(t)}\n",
    "$$\n",
    "\n",
    "Note:\n",
    "- The covariance between two series can be calculated using the ```series1.cov(series2)``` function.\n",
    "- The standard deviation of a series can be calculated using the ```series.std()``` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calc_corr(predictions, truth):\n",
    "    # Again, the data must be aligned for this calculation\n",
    "    truth, predictions = truth.align(predictions, join='inner')\n",
    "    corr = ...\n",
    "    return corr\n",
    "\n",
    "corr = calc_corr(satellite_ts, insitu_wse)\n",
    "print(f'The Pearson Correlation between the satellite and in-situ data is {corr:.3f}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's compare the Quality with the [DAHITI timeseries](https://dahiti.dgfi.tum.de/en/10246/water-level-altimetry/):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dahiti_wse = pd.read_csv('data/SamRayburn/dahiti_wse.csv', comment='#', delimiter=';', usecols=[0,1], parse_dates=[0], index_col=0).water_level\n",
    "dahiti_wse = dahiti_wse.resample('1D').median().dropna()\n",
    "dahiti_wse.plot(figsize=(12, 5))\n",
    "plt.ylabel('Water Surface Elevation [m]')\n",
    "plt.title('Figure 6: Water Surface Elevation measured at the DAHITI station')\n",
    "plt.grid()\n",
    "plt.show()\n",
    "\n",
    "dahiti_bias = (dahiti_wse - insitu_wse).median()\n",
    "dahiti_wse = dahiti_wse - dahiti_bias\n",
    "print(f'The bias between DAHITI and the in-situ data is {bias:.3f} meters')\n",
    "rmse_dahiti = calc_rmse(dahiti_wse, insitu_wse)\n",
    "print(f'The RMSE between DAHITI and the in-situ data is {rmse_dahiti:.3f} meters')\n",
    "nse_dahiti = calc_nse(dahiti_wse, insitu_wse)\n",
    "print(f'The NSE between DAHITI and the in-situ data is {nse_dahiti:.3f}')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "base",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
